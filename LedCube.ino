#include <TinyWireM.h> //ATtiny85, 16 MHz internal
#include "PCF8574_Attiny.h" // Required for PCF8574

PCF8574 ex1;
PCF8574 ex2;
PCF8574 ex3;

bool ledBox[4][4][4]= //do dynamicznej zmiany led (na później)
{{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}},
{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}},
{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}},
{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}}};

void setup() {

  ex1.begin(0x20);
  ex2.begin(0x21);
  ex3.begin(0x22);
   
  ex1.pinMode(0, OUTPUT);//GND LED
  ex1.pinMode(1, OUTPUT);
  ex1.pinMode(2, OUTPUT);
  ex1.pinMode(3, OUTPUT);
  ex1.pinMode(4, OUTPUT);
  ex1.pinMode(5, OUTPUT);
  ex1.pinMode(6, OUTPUT);
  ex1.pinMode(7, OUTPUT);
  
  ex2.pinMode(0, OUTPUT);//GND LED
  ex2.pinMode(1, OUTPUT);
  ex2.pinMode(2, OUTPUT);
  ex2.pinMode(3, OUTPUT);
  ex2.pinMode(4, OUTPUT);
  ex2.pinMode(5, OUTPUT);
  ex2.pinMode(6, OUTPUT);
  ex2.pinMode(7, OUTPUT);

  ex3.pinMode(0, INPUT_PULLUP);//Guzik 1 (liczone od złącza zasilania)
  ex3.pinMode(1, INPUT_PULLUP);//Guzik 2
  ex3.pinMode(2, INPUT_PULLUP);//Guzik 3
  ex3.pinMode(3, OUTPUT);//---------------Niepodłączone
  ex3.pinMode(4, OUTPUT);//1 parter (VCC LED)
  ex3.pinMode(5, OUTPUT);//2
  ex3.pinMode(6, OUTPUT);//3
  ex3.pinMode(7, OUTPUT);//4 szczyt

  //Set - 1
  //Clear - 0
  
  ex1.set();//gnd stering
  ex2.set();//gnd stering
  //ex3.clear();//vcc stering
  ex3.digitalWrite(4,LOW);
  ex3.digitalWrite(5,LOW);
  ex3.digitalWrite(6,LOW);
  ex3.digitalWrite(7,LOW);
  
  //ex1.digitalWrite(0, HIGH);
  delay(1000);
}


void test(){
  //1. zapalenie i gaszenie kolejnych poziomów led
  //2. zapalenie i gaszenie kolejnych pionów led
  //3. zapalenie i zgaszenie wszystkich ledów
  
  //ekwencja 1
  ex1.clear();
  //ex1.digitalWrite(0, HIGH);

  ex2.clear();
  //ex2.digitalWrite(0, HIGH);

  ex3.digitalWrite(4, HIGH);
  delay(1000);
  ex3.digitalWrite(4, LOW);
  ex3.digitalWrite(5, HIGH);
  delay(1000);
  ex3.digitalWrite(5, LOW);
  ex3.digitalWrite(6, HIGH);
  delay(1000);
  ex3.digitalWrite(6, LOW);
  ex3.digitalWrite(7, HIGH);
  delay(1000);
  ex3.digitalWrite(7, LOW);
  delay(1000);
  ex1.set();
  ex2.set();

  //ekwencja 2
  ex3.digitalWrite(4,HIGH);
  ex3.digitalWrite(5,HIGH);
  ex3.digitalWrite(6,HIGH);
  ex3.digitalWrite(7,HIGH);
  
  ex1.digitalWrite(6,LOW);
  ex1.digitalWrite(7,LOW);
  ex1.digitalWrite(0,LOW);
  ex1.digitalWrite(1,LOW);
  delay(1000);
  ex1.set();
  ex1.digitalWrite(4,LOW);
  ex1.digitalWrite(5,LOW);
  ex1.digitalWrite(2,LOW);
  ex1.digitalWrite(3,LOW);
  delay(1000);
  ex1.set();
  ex2.digitalWrite(6,LOW);
  ex2.digitalWrite(7,LOW);
  ex2.digitalWrite(0,LOW);
  ex2.digitalWrite(1,LOW);
  delay(1000);
  ex2.set();
  ex2.digitalWrite(4,LOW);
  ex2.digitalWrite(5,LOW);
  ex2.digitalWrite(2,LOW);
  ex2.digitalWrite(3,LOW);
  delay(1000);
  ex2.set();
  ex3.digitalWrite(4,LOW);
  ex3.digitalWrite(5,LOW);
  ex3.digitalWrite(6,LOW);
  ex3.digitalWrite(7,LOW);

  //ekwencja 3
  ex1.clear();
  ex2.clear();
  ex3.digitalWrite(4,HIGH);
  ex3.digitalWrite(5,HIGH);
  ex3.digitalWrite(6,HIGH);
  ex3.digitalWrite(7,HIGH);
  delay(500);
  ex1.set();
  ex2.set();
  ex3.digitalWrite(4,LOW);
  ex3.digitalWrite(5,LOW);
  ex3.digitalWrite(6,LOW);
  ex3.digitalWrite(7,LOW);
}

void tracer(){  //losowe zapalanie i gaszenie pojedyńczej diody (czas świecenia również losowy)
  while(ex3.digitalRead(2)==1){
    int vertical=random(0,15);
    int horizontal=random(4,7);
    int del=random(100,1000);

    if(vertical<=7){
      ex1.digitalWrite(vertical,LOW);
    }else{
      ex2.digitalWrite((vertical-8),LOW);
    }
    ex3.digitalWrite(horizontal,HIGH);
    delay(del);

    if(vertical<=7){
      ex1.digitalWrite(vertical,HIGH);
    }else{
      ex2.digitalWrite((vertical-8),HIGH);
    }
    ex3.digitalWrite(horizontal,LOW);
  }
}

void refresh(){//pomyśleć o bluetooth lub podobnym do dynamicznej zmiany ledów
//  ex1.digitalWrite(i, ledBox[j][k][0]);
//  ex2.digitalWrite(i, ledBox[j][k][0]);
//  ex3.digitalWrite(i, ledBox[j][k][0]);
}

/*
ex1   ex2
0 1   0 1
2 3   2 3
4 5   4 5
6 7   6 7
 *
ex3
3
2
1
0
GND
*/

void loop() {

if(ex3.digitalRead(0)==0){
  test();
  delay(100);
}
if(ex3.digitalRead(1)==0){
  tracer();
  delay(100);
}
 
//expander.toggle(0);
//expander.write(255); // All led off
//expander.write(0);   // All led on
//expander.set();   // All led off
//expander.clear(); // All led on
}
