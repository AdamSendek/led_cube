# Led_Cube
Last update: 13-10-2019

## Description
Decorative Led Cube

## Hardware
* ATtiny85    1pcs  
* PCF8574     3pcs  
* Tact switch 3pcs  
* Socket dc   1pcs  
* Transistors BC337-40 4pcs  
* Rezistors   10 ohm 1% 4pcs  
* Rezistors   10k ohm 1% 3pcs  
* Capacitors ceramic 100nF 2pcs  
* Capacitors ceramic 330nF 1pcs  
* Power Supply 12v 0.5A 1pcs
* Voltage Regulator L78S05CV 1pcs  
* Switch on/off 1pcs  
* Grean LED 64pcs 
* Silver-plated copper wire fi 0.7mm ~8m   
  
### 3D Print
PCB Support  
https://www.thingiverse.com/thing:4981643  
  
PCB Board Connector  
https://www.thingiverse.com/thing:4981647  
  
## Software
### Installation
Arduino IDE + USBasp (or difrent programmer)

## Usage
Button 1 - Test all led, first horizontally, after vertical, at the end all led blink  
Button 2 - Random led blink  
Button 3 - break sequence Buttom 2  

## License
Creative Commons Attribution-NonCommercial-ShareAlike 4.0

## Photos
![](/Cube_Led_End.jpg)
